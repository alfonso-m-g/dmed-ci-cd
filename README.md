# The REST API was created using Retool
URL:
https://retool.com/utilities/generate-api-from-csv
# Production environment URL:
http://dmed-rest-api.surge.sh/
# Development environment URL:
http://dmed-rest-api-dev.surge.sh/

This is the repo for group #4